//
//  CustomTestMigrationPolicy.m
//  testMigration
//
//  Created by Andre Vellori on 07/04/2015.
//  Copyright (c) 2015 Andre Vellori. All rights reserved.
//

#import "CustomTestMigrationPolicy.h"
#import "TEST.h"

@implementation CustomTestMigrationPolicy

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError *__autoreleasing *)error
{
    NSLog(@"Save this --> %@", [sInstance valueForKey:@"field2"]);
    return [super createDestinationInstancesForSourceInstance:sInstance entityMapping:mapping manager:manager error:error];
}

@end
