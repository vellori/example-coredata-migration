//
//  ViewController.m
//  testMigration
//
//  Created by Andre Vellori on 07/04/2015.
//  Copyright (c) 2015 Andre Vellori. All rights reserved.
//

#import "ViewController.h"
#import "CoreDataTestSuite.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self checkData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) checkData
{
    CoreDataTestSuite *testSuite = [ CoreDataTestSuite new];
    [testSuite checkDataAndCreate];
}

@end
