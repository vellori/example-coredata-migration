//
//  AppDelegate.h
//  testMigration
//
//  Created by Andre Vellori on 07/04/2015.
//  Copyright (c) 2015 Andre Vellori. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;




@end

