//
//  CustomTestMigrationPolicy.h
//  testMigration
//
//  Created by Andre Vellori on 07/04/2015.
//  Copyright (c) 2015 Andre Vellori. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface CustomTestMigrationPolicy : NSEntityMigrationPolicy

@end
