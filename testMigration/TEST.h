//
//  TEST.h
//  testMigration
//
//  Created by Andre Vellori on 07/04/2015.
//  Copyright (c) 2015 Andre Vellori. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TEST : NSManagedObject

@property (nonatomic, retain) NSString * field1;
@property (nonatomic, retain) NSString * field3;

@end
