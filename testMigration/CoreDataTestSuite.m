//
//  CoreDataTestSuite.m
//  testMigration
//
//  Created by Andre Vellori on 07/04/2015.
//  Copyright (c) 2015 Andre Vellori. All rights reserved.
//

#import "CoreDataTestSuite.h"
#import <MagicalRecord.h>
#import <CoreData+MagicalRecord.h>

#import "TEST.h"

@implementation CoreDataTestSuite

- (id) init
{
    self = [super init];
    if (self)
    {
        [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"TEST"];
    }
    return self;
}

- (void) checkDataAndCreate
{
    NSArray *array = [TEST MR_findAll];
    
    if (array.count == 0)
    {

        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            NSLog(@"Array is empty, creating basic object.");
            for (NSInteger i=0;i<5;i++)
            {
                TEST *aTest = [TEST MR_createInContext:localContext];
                aTest.field1 = [[NSDate date] description];

                //aTest.field2 = [NSString stringWithFormat:@"Field 2: %ld",i];

                aTest.field3 = [NSString stringWithFormat:@"Field 3: %ld",i];
            }
            
            
        } completion:^(BOOL success, NSError *error) {
            NSLog(@"Saved? %@", success?@"YES":@"NO");
            if (!success) {
                NSLog(@"Because %@", error);
            }
        }];
    } else {
                NSLog(@"Array is full,skipping object creation.");
    }
    
}

@end
