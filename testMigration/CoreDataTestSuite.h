//
//  CoreDataTestSuite.h
//  testMigration
//
//  Created by Andre Vellori on 07/04/2015.
//  Copyright (c) 2015 Andre Vellori. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataTestSuite : NSObject
- (void) checkDataAndCreate;
@end
